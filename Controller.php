<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App;

class Controller extends BaseController {
    protected $className;
    protected $model;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        $this->model = "App\\" . $this->className;
    }

    public function index() {
        return view($this->className.'/index');
    }

    public function fetch(Request $request) {
        if($request->ajax()) {
            $recs = $this->model::search($request->input('keywords') ?: '');
            $recs = $recs->paginate(1000);

            return response()->json(['collection' => $recs]);
        }
        return false;
    }

    public function add(Request $request) {
        if($request->ajax()) {
            $validator = Validator::make(request()->all(), [
                'title' => 'required|string|max:255'
            ]);

            if($validator->fails()) {
                return response()->json(['errors' => $validator->errors()]);
            }

            $new_rec = $this->model::create([
                'title'     => $request->input('title')
            ]);

            return response()->json([
                'addFields'     => [
                    'id'        => $new_rec->id
                ]
            ]);
        }
        return false;
    }

    public function edit(Request $request) {
        if($request->ajax()) {
            $validator = Validator::make(request()->all(), [
                'id'    => 'required|numeric',
                'title' => 'required|string|max:255'
            ]);

            if($validator->fails()) {
                return response()->json(['errors' => $validator->errors()]);
            }

            $this->model::find($request->input('id'))->update([
                'title'     => $request->input('title')
            ]);

            return response()->json(['msg' => 'Record Updated.']);
        }
        return false;
    }

    public function update(Request $request) {
        if($request->ajax()) {
            $updated = $this->model::whereIn('id', (array)$request->input('ids'))
                            ->update($request->input('fields'));

            return response()->json([
                'result' => (bool) $updated
            ]);
        }
        return false;
    }

    public function delete(Request $request) {
        if($request->ajax()) {
            $jsonResponse = $this->model::destroy($request->input('ids')) ? ['successMsg' => $this->className.' deleted successfully.'] : ['errorMsg' => 'Error deleting '.$this->className];
            $this->afterDeleteHook($request->input('ids'));
            return response()->json($jsonResponse);
        }
        return false;
    }

    public function afterDeleteHook($ids) {
        //Delete related notifications
        \App\Notification::where(DB::raw('json_extract(data, "$.model")'), $this->className)
            ->whereIn(DB::raw('json_extract(data, "$.model_id")'), $ids)
            ->delete();
    }
}
