require('../bootstrap');

import { MessageBox } from '../MessageBox.js';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Vue2Filters from 'vue2-filters';
Vue.use(Vue2Filters);

const router = new VueRouter({
	mode: "history",
	routes: [
		{
			path: "/admin",
			component: () => import("./admin/Dashboard.vue" /* webpackChunkName: "js/admin/components/dashboard" */ )
		},
		{
			path: "/prospects",
			component: () => import("./admin/Prospects.vue" /* webpackChunkName: "js/admin/components/prospects" */ )
		},
		{
			path: "/food-cats",
			component: () => import("./admin/FoodCats.vue" /* webpackChunkName: "js/admin/components/foodcats" */ )
		},
		{
			path: "/base-categories",
			component: () => import("./admin/BaseCats.vue" /* webpackChunkName: "js/admin/components/basecategories" */ )
		},
		{
			path: "/base-crusts",
			component: () => import("./admin/BaseCrusts.vue" /* webpackChunkName: "js/admin/components/basecrusts" */ )
		},
		{
			path: "/base-toppings",
			component: () => import("./admin/BaseToppings.vue" /* webpackChunkName: "js/admin/components/basetoppings" */ )
		},
		{
			path: "/zip-codes",
			component: () => import("./admin/ZipCodes.vue" /* webpackChunkName: "js/admin/components/zipcodes" */ )
		},
		{
			path: "/states",
			component: () => import("./admin/States.vue" /* webpackChunkName: "js/admin/components/states" */ )
		},
		{
			path: "/regions",
			component: () => import("./admin/Regions.vue" /* webpackChunkName: "js/admin/components/regions" */ )
		},
		{
			path: "/users",
			component: () => import("./admin/Users.vue" /* webpackChunkName: "js/admin/components/users" */ )
		},
		{
			path: "/feedback",
			component: () => import("./admin/Feedback.vue" /* webpackChunkName: "js/admin/components/feedback" */ )
		}
	],
	scrollBehavior (to, from, savedPosition) {
		return savedPosition || { x: 0, y: 0 };
	}
});

//const store = new Vuex.Store();

var app = new Vue({
	el: '#app',
	router,
	//store,
	created() {
		axios.get(`/user/currentUser`)
			.then(r => {
				if(r.status == 200) {
					this.user = r.data.user;

					window.Echo.private(`App.User.${this.user.id}`)
						.notification(n => {
							this.notifications.msgs.push({
								data: {
									action_url: n.action_url,
									content:	n.content,
									model_id:	n.model_id,
									title:		n.title,
									model:		n.model
								},
								type: 	n.type,
								id: 	n.id

							});
						});
				}
			});

		this.getAdminNotifications();
	},
	data: () => ({
		modelName: "",
		modelNamePlural: "",
		form: {
			inputs: {}
    	},
    	formSearch: {
    		inputs: {}
    	},
    	toolbar: {
    		btns: [],
    		menuItems: []
    	},
    	cache: {},
    	selected: [],
    	formDialog: false,
		searchDialog: false,
		formMode: 'add',
		notifications: {
			drawer: false,
			msgs: []
		},
		navMenu: null,
        navMenuItems: [
	        { text: 'Dashboard Home', icon: 'fas fa-home', url: '/admin' },
	        {
	        	text: 'Users',
	        	icon: 'fas fa-users',
	            model: true,
	            subMenuItems: [
	              { text: 'List Users', icon: 'fas fa-users', url: '/users' },
	              { text: 'List Payment Profiles', icon: 'fas fa-credit-card', url: '/payment-profiles' }
	            ]
	        },
	        {
	        	text: 'Advertisers',
	        	icon: 'fas fa-utensils',
	            model: false,
	            subMenuItems: [
	              { text: 'List Advertisers', icon: 'fas fa-building', url: '/advertiser' },
	              { text: 'List Restaurants', icon: 'fas fa-truck', url: '/restaurants' }
	            ]
	        },
	        { text: 'Orders', icon: 'fas fa-shopping-cart', url: '/orders' },
	        {
	        	text: 'Base Options',
	        	icon: 'fas fa-list-alt',
	            model: false,
	            subMenuItems: [
	              { text: 'Base Category List', url: '/base-categories' },
	              { text: 'Base Crust List', url: '/base-crusts' },
	              { text: 'Base Topping List', url: '/base-toppings' }
	            ]
	        },

	        { text: 'Food categories', icon: 'fas fa-utensils', url: '/food-cats' },
	        { text: 'Food Banners', icon: 'fas fa-newspaper', url: '/banners' },
	        {
	        	text: 'Locales',
	        	icon: 'fas fa-globe',
		        model: false,
		        subMenuItems: [
		            { text: 'Zip Codes List / Tax Rates', icon: 'fas fa-percent', url: '/zip-codes' },
		            { text: 'States List', icon: 'fas fa-map', url: '/states' },
		            { text: 'Regions List', icon: 'fas fa-globe', url: '/regions' }
		        ]
	        },
	        {
	        	text: 'Tutorials',
	        	icon: 'fas fa-question-circle',
	        	model: false,
	        	subMenuItems: [
		            { text: 'Tutorial List', icon: 'fas fa-question-circle', url: '/tutorials' },
		            { text: 'Add Tutorial', icon: 'fas fa-plus-square', url: '/tutorial-add' }
	            ]
	        },
	        { text: 'Leads/Prospects List', icon: 'fas fa-address-card', url: '/prospects' },
	        { text: 'Feedback/Contact Inquiries', icon: 'fas fa-address-book', url: '/feedback' },
	        { text: 'Settings', icon: 'fas fa-cogs', url: '/settings' }
        ]
	}),
	methods: {
		logout() {
			MessageBox({
				title: `<i class='fas fa-sign-out-alt orange--text text--darken-2'></i> Are you sure you want to logout?`,
	            buttons: "[No][Yes]"
	        }, (e) => {
	        	if("Yes" == e) {
	        		$('#logoutForm').submit();
	        	}
	        });
		},
		// redefined/reassigned in each of the components
		fetch() { },
		search() { },
		add() { },
		save() { },
		deleteSelected() {},

		getAdminNotifications() {
			axios.get(`/notifications/fetch`)
				.then(r => {
					if(r.status == 200) {
	    				this.notifications.msgs = r.data.notifications;
	    			}
				});
		},
		markNotificationRead(id) {
			axios.patch(`/notification/markRead`, { id: id })
    			.then(r => {
    				if(r.status == 200) {
    					this.notifications.msgs.splice(
	            			this.notifications.msgs.indexOf(
	            				this.notifications.msgs.filter(row => row.id == id)[0]
	            			), 1);
    				}
				});
		}
	}
});
