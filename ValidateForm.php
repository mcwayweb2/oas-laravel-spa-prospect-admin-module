<?php

namespace App\Http\Requests\Prospect;

use Illuminate\Foundation\Http\FormRequest;

class ValidateForm extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {
        $params = [
            'name'          => 'required|string|max:255',
            'food_cat_id'   => 'required|numeric',
            'address'       => 'required|string|max:255',
            'city'          => 'required|string|max:255',
            'state_id'      => 'required|numeric',
            'zip'           => ['regex:/\b\d{5}\b/', 'required'],
            'contact_fname' => 'string|max:255|nullable',
            'contact_lname' => 'string|max:255|nullable',
            'contact_title' => 'string|max:255|nullable',
            'phone'         => ['regex:/^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$/', 'required'],
            'website'       => 'string|max:255|nullable',
            'address2'      => 'max:255:nullable',
            'notify'        => 'nullable',
            'response_notes'=> 'nullable'
        ];

        if($this->getMethod() == "PUT") {
            $params['id'] = 'required|numeric';
        }

        return $params;
    }

    public function messages() {
        return [
            'food_cat_id.required'  => 'You must select a food category.',
            'state_id.required'     => 'You must select your state.'
        ];
    }

}
