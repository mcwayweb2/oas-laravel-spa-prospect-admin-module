<?php

namespace App\Notifications\Admin;

use App\Prospect;

class ProspectAddedNotification extends AdminBaseNotification {
    private $prospect;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Prospect $prospect) {
        $this->prospect = $prospect;

        $content = "{$this->prospect->address} {$this->prospect->address2}<br />" .
        "{$this->prospect->city}, {$this->prospect->state->abbr} {$this->prospect->address}";

        $this->fields = [
            'model'     => 'Prospect',
            'model_id'  => $this->prospect->id,
            'action_url'=> '/prospects',
            'title'     => "New {$this->prospect->foodcat->title} Prospect",
            'content'   => $content
            ];
    }
}
