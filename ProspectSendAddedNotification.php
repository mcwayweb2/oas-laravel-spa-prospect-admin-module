<?php

namespace App\Listeners\Admin;

use App\Events\Admin\ProspectAddedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Admin\ProspectAddedNotification;

class ProspectSendAddedNotification {
    public $queue = 'notifications.admin';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param  ProspectAdded  $event
     * @return void
     */
    public function handle(ProspectAddedEvent $event) {
        //send to all admins
        Notification::send(\App\User::role('admin')->get(), new ProspectAddedNotification($event->prospect));
    }
}
